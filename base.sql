drop database if exists ind_techs;
create database ind_techs;
use ind_techs;

create table categories (
	id int not null auto_increment primary key,
	parent_id int default null,
	label tinytext not null,
	foreign key (parent_id)
		references categories(id)
		on delete cascade
		-- or set null, what you prefer
);

create table users (
	id int not null auto_increment primary key,
    login tinytext not null,
    password tinytext not null
);

create table products (
	id int not null auto_increment primary key,
	cat_id int not null,
	label tinytext,
	price float,
	foreign key (cat_id)
		references categories(id)
		on delete cascade
);


create table orders (
	id int not null auto_increment primary key,
	user_id int,
	ordered_at datetime not null default now(),
	foreign key (user_id)
		references users(id)
		on delete set null
);

create table order_products (
	id int not null auto_increment,
	order_id int not null,
	product_id int not null,
	primary key(id),
	foreign key(order_id)
		references orders(id)
		on delete cascade,
	foreign key(product_id)
		references products(id)
		on delete cascade
);

insert into users (login, password) values
	('ydz', 'pwd'), ('user', 'pwd');

insert into categories (parent_id, label) values
	(null, 'Category 1'),
	(null, 'Category 2'),
	(null, 'Category 3'),
	(1, 'Category 11'),
	(1, 'Category 12'),
	(5, 'Category 121'),
	(5, 'Category 122'),
	(2, 'Category 21'),
	(3, 'Category 31');

insert into products (cat_id, label, price) values
	(1, 'Product 1', 100.0),
	(1, 'Product 2', 110.0),
	(2, 'Product 3', 235.80),
	(2, 'Product 4', 400.40),
	(3, 'Product 5', 195.15),
	(3, 'Product 6', 215.45),
	(4, 'Product 7', 900.0),
	(5, 'Product 8', 100.0),
	(6, 'Product 9', 600.0),
	(8, 'Product 10', 118.0),
	(9, 'Product 11', 45.0);