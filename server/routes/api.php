<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Product;
use App\Category;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', function() {
    $result = array();
    return Product::all()->map(function($product) {
        return array(
            'id' => $product->id,
            'label' => $product->label,
            'cat_id' => $product->category->id,
            'category' => $product->category->label,
            'price' => $product->price
        );
    });
});

Route::get('/categories', function() {
    return Category::all();
});

Route::post('/products/update', 'EditorController@updateProduct');

Route::delete('/products', 'EditorController@removeProduct');

Route::post('/products/create', 'EditorController@createProduct');