@extends('layout')
@section('title', 'Editor')
@section('page', 'Editor')
@section('content')
	<div id='editor'>
		<h2>Editor</h2><br/>

		<table class="products table table-hover table-condensed">
			<thead>
				<th>ID</th>
				<th>Label</th>
				<th>Category label</th>
				<th>Price</th>
				<th>Action</th>
			</thead>
			<tbody>
				@verbatim
					<tr v-for="product in products" :key="product.id">
						<td class="field">{{ product.id }}</td>
						<td class="field">{{ product.label }}</td>
						<td class="field">
							{{ product.category }}
						</td>
						<td class="field">{{ product.price }}</td>
						<td class="actions" v-bind:data-product-id="product.id">
							<div
								class="btn btn-xs btn-primary"
								v-on:click="editProduct($event)">
								<span class="fa fa-edit"></span>
							</div>
							<div
								class="btn btn-xs btn-danger"
								v-on:click="removeProduct($event)">
								<span class="fa fa-remove"></span>
							</div>
						</td>
					</tr>
				@endverbatim
			</tbody>
		</table>
		<div class="btn btn-block btn-sm btn-warning"
			v-on:click="createProduct">
			Create
		</div>
		<div class="modal fade" tabindex="-1" id="edit-modal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span>&times;</span>
						</button>
						<h4 class="modal-title">Edit product</h4>
					</div>
					<div class="modal-body">
						<form>
							<div class="form-group">
								<label class="control-label">Label</label>
								<input type="text" class="form-control" placeholder="Product label"
									v-model="editing.label"
								>
							</div>
							<div class="form-group">
								<label class="control-label">Category</label>
								<div class="category-select">
									<categories-tree
										v-bind:current="editing.cat_id"
										v-bind:categories="categories"
										v-on:select="changeCategory">
									</category-tree>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Price</label>
								<input type="text" class="form-control" placeholder="Product price"
									v-model="editing.price"
								>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<div class="btn btn-default" data-dismiss="modal">
							Close
						</div>
						<div class="btn btn-primary" v-on:click="updateProduct">
							Save changes
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection