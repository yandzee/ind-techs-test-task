<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Test App - @yield('title')</title>
        <link href="css/app.css" rel="stylesheet" type="text/css">
        <script src="js/app.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="main">
            <div class="sidebar">
                Test Task - @yield('page')
            </div>
            <div class="content">
                @yield('content')
            </div>
        </div>
    </body>
</html>
