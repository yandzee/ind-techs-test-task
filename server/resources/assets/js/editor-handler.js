
Vue.component('categories-tree', require('./components/categories-tree.vue'));


function loadCategories() {
	return http.get('api/categories')
		.then(r => r.data)
		.then(categories => {
			let stack = categories.filter(c => c.parent_id == null);
			let roots = stack.slice();
			while (stack.length !== 0) {
				let node = stack.pop();
				let children = categories.filter(cat => cat.parent_id === node.id);
				categories = categories.filter(cat => cat.parent_id !== node.id);
				node.categories = children;
				stack = stack.concat(children);
			}
			return roots;
		});
}

function _updateProduct(editing) {
	return http.post('api/products/update', editing);
}

function _removeProduct(toRemove) {
	console.log(toRemove);
	return http.delete('api/products', {
		params: toRemove
	});
}

function _createProduct() {
	return http.post('api/products/create').then(r => r.data);
}

$('#editor').ready(_ => {
	let editor = new Vue({
		el: '#editor',
		data: {
			products: [],
			categories: [],
			editing: {}
		},
		methods: {
			currentProduct(e) {
				let productId = $(e.target).closest('td').data('product-id');
				let product = this.products.find(p => p.id === productId);
				return product;
			},
			editProduct(e) {
				let product = this.currentProduct(e);
				if (!product) {
					return;
				}
				this.openEditModal(product);
			},
			openEditModal(product) {
				this.editing = Object.assign({}, product);
				$('#edit-modal').modal('show');
			},
			removeProduct(e) {
				let product = this.currentProduct(e);
				console.log(this.products);
				if (!product) { return }

				_removeProduct(product).then(_ => {
					refreshProducts();
				});
			},
			updateProduct() {
				_updateProduct(this.editing).then(r => {
					refreshProducts();
					$('#edit-modal').modal('hide');
				});
			},
			createProduct() {
				_createProduct().then(product => {
					console.log('created product: ', product);
					refreshProducts();
					return product;
				}).then(product => {
					this.openEditModal(product);
				});
			},
			changeCategory(c) {
				this.editing.cat_id = parseInt(c);
			}
		}
	});
	function refreshProducts() {
		return http.get('api/products').then(r => r.data).then(products => {
			editor.products = products;
		});
	}
	function refreshCategories() {
		return loadCategories().then(categories => {
			console.log('categories loaded: ', categories);
			editor.categories = categories;
		});
	}
	refreshProducts();
	refreshCategories();
});