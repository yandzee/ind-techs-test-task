<?php
    namespace App\Http\Controllers;

    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Http\Request;
    use App\Product;
    use App\Category;

    class EditorController extends Controller {
        public function __construct() { }

        public function render() {
            return view('editor');
        }

        public function updateProduct(Request $request) {
            $product = Product::find($request->id);
            $product->label = $request->label;
            $product->price = $request->price;
            $product->cat_id = $request->cat_id;
            $product->save();

            return $product;
        }

        public function removeProduct(Request $request) {
            Product::find($request->id)->forceDelete();
        }

        public function createProduct(Request $request) {
            $product = new Product;
            $product->cat_id = 1;
            $product->label = 'New Product';
            $product->price = 0.0;
            $product->save();

            return $product;
        }

    }
?>